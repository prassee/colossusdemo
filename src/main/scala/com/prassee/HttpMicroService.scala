package com.prassee

import colossus._
import colossus.protocols.http._
import colossus.protocols.http.HttpMethod._
import colossus.protocols.http.UrlParsing._
import colossus.service._
import java.io.File
import scala.io.Source
import java.io.FileWriter

/*
 * 
 */
object HttpMicroService extends App {

  implicit val io_system = IOSystem()

  val responseTypes = List(("Content-Type", "application/json"))

  Service.serve[Http]("KVStore", 10200) {
    context =>
      val kvs = KVStoreBackend
      context.handle {
        connection =>
          connection.become {
            case request@Get on Root =>
              Callback.successful(request.ok("{\"sim\" : \"data\"}",
                responseTypes))
            case request@Get on Root / "api" / "create" / store =>
              Callback.successful(request.ok("{\"status\" : \"" + kvs.createStore(store) + "\"}",
                responseTypes))
            case request@Get on Root / "api" / store / key / value =>
              Callback.successful(request.ok("{\"status\" : \"" + "saved" + "\"}",
                responseTypes))
            case request@Get on Root / "api" / store / key =>
              Callback.successful(request.ok("{\"result\" : \"" + kvs.readValue(store, key) + "\"}",
                responseTypes))
          }
      }
  }

}

/*
 * 
 */
object KVStoreBackend {
  lazy val baseDir = this.getClass.getResource("/").getPath

  createStore("kvs")

  def createStore(name: String) = new File(baseDir + "/" + name).createNewFile()

  def getStore(name: String) = new File(baseDir + "/" + name)

  def writeKv(store: String, key: String, value: String) = {
    val file = new FileWriter(getStore(store), true)
    file.write(s"""$key->$value""")
    file.write("\n")
    file.close()
  }

  def readValue(store: String, key: String): String = {
    val file = Source.fromFile(getStore(store))
    file.getLines().filter { x => x.split("->")(0).equals(key) }.toList.head.split("->")(1)
  }
}
