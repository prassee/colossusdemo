package com.prassee

import colossus.IOSystem
import colossus.protocols.telnet.Telnet
import colossus.protocols.telnet.TelnetCommand
import colossus.protocols.telnet.TelnetReply
import colossus.service.Callback
import colossus.service.Service

/*
 * 
 */
object TelnetMicroService extends App {
  implicit val io_system = IOSystem()

  Service.serve[Telnet]("name", 10100) { context =>
    {
      context.handle {
        connection =>
          connection.become {
            case TelnetCommand("say" :: text :: Nil) => {
              Callback.successful(TelnetReply(text))
            }
            case TelnetCommand("exit" :: Nil) => {
              connection.gracefulDisconnect()
              Callback.successful(TelnetReply("bye"))
            }
          }
      }
    }
  }

}