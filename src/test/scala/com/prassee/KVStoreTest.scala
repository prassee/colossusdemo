package com.prassee

import org.scalatest._

class KVStoreTest extends FlatSpec with ShouldMatchers {

  val kvStore = KVStoreBackend

  "KVstore" should "create a new file to crate a new store " in {
    // kvStore.createStore("testKv").shouldBe(true)
  }

  it should "create a new key value entry " in {
    kvStore.writeKv("testKv", "key", "value")
  }
  
  it should "get new key value entry " in {
    kvStore.readValue("testKv", "key").shouldBe("value")
  }
}